/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server5htcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author STUDENTE
 */
public class Server5HTCP {

    /**
     * @param args the command line arguments
     */
    public static final int port=6666;
    public static void main(String[] args) throws IOException {
        
        // creo il socket server sulla porta 
        ServerSocket serverSocket=new ServerSocket(port);
        while(true){
            // rimango in attesa di una connessione
            System.out.println("In attesa di una connessione...");
            Socket connectedSocket=serverSocket.accept();
            System.out.println("Connesso nuovo client.");

            // creo l'istanza della classe ConnectionHandler che estende Thread
            ConnectionHandler connHandler=new ConnectionHandler(connectedSocket);
            // eseguo il metodo "run" attraverso start()
            // questo esegue le istruzioni di run su thread separato
            connHandler.start();
        }
        
        
        
        
    }
    
}
