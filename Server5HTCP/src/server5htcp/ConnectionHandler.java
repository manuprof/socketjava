/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server5htcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author STUDENTE
 */
public class ConnectionHandler extends Thread {

    private Socket connectedSocket;

    // passo la socket connessa
    public ConnectionHandler(Socket socket) {
        this.connectedSocket = socket;
    }

    @Override
    // questo verrà eseguito su thread separato
    public void run() {
        try {
            // recupero il messaggio dallo stream in ingresso
            BufferedReader in = new BufferedReader(new InputStreamReader(connectedSocket.getInputStream()));
            // stampo il messaggio ricevuto
            System.out.println(in.readLine());
            System.out.println("Fine!");
        } catch (IOException ex) {
            System.err.println(ex.toString());
        }
        finally{
            try {
                this.connectedSocket.close();
            } catch (IOException ex) {
                Logger.getLogger(ConnectionHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
    }

}
