/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client5htcp;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import javax.swing.JOptionPane;

/**
 *
 * @author STUDENTE
 */
public class Client5HTCP {

    /**
     * @param args the command line arguments
     */
    // dichiaro la porta su cui sta in ascolto il server
    public static final int port=6666;
    
    public static void main(String[] args) throws IOException {
        // cerco di connettermi al server
        System.out.println("Connessione al server in corso...");
        Socket clientSocket=new Socket("localhost",port);
        System.out.println("Connesso!");
        // proviamo ad inviare un messaggio
        PrintWriter out=new PrintWriter(clientSocket.getOutputStream(), true);
        // scrivo il messaggio
        String msg=JOptionPane.showInputDialog("Inserisci il messaggio");
        out.println(msg);
        
        System.out.println("Comunicazione terminata");
        
        clientSocket.close();
        
        
    }
    
}
